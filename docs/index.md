# Groupe et présentation de projet

## 1. À propos de notre équipe

Bonjour à tous ! Il est maintenant l'heure de vous présenter notre équipe (groupe n°3) pour le projet final du cours [« How To Make (almost) Any Experiments »](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/#ulb-how-to-make-almost-any-experiments-fabzero-inside).

Notre team se compose de :

- [Juliette Debure](https://juliette-debure-fablab-ulb-enseignements-2023-20-05978f45637b28.gitlab.io/), étudiante en bioingénieur
- [Eva Pueyo Albo](https://eva-pueyoalbo-fablab-ulb-enseignements-2023-2024-ebf9b3ccbd7a5a.gitlab.io/), étudiante en bioingénieur
- [Armand Bobelea](https://armand-bobelea-fablab-ulb-enseignements-2023-202-520adf8d6939fd.gitlab.io/), étudiant en biologie
- [Timo Pambou](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/timo.pambou), étudiant en bioingénieur

Nous voilà :)

![](./images/Photo_de_famille_3.jpg)



Notre équipe se compose de trois bioingénieurs et d'un biologiste. Une pluridisciplinarité est un avantage considérable pour les travaux de groupe. Cette diversité nous permet d'avoir des points de vue différents sur les sujets abordés, ce qui entraine des débats continus et mouvementés. Vous pouvez, néanmoins, remarquer le préfixe bio dans chacun de nos choix d'étude, ce qui met le doigt sur nos ambitions et intérêts communs. 

## 2. L'abstract de notre projet

![](./images/Page_de_garde.jpg)

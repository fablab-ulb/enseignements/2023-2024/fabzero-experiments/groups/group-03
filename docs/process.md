# Processus 

Bienvenu sur notre site ! 

Sur cette page, vous pouvez suivre l'évolution de notre projet jusqu'à son aboutissement. 

## 1. La problématique 

Pour commencer, il nous a fallu choisir une problématique dont nous nous soucions tous. La thématique proposée par Denis est "**Rendre l'invisible visible**". Nous avons alors réalisé un brainstorming reprenant toutes nos idées. Le domaine de la **pollution des eaux** ressortait énormément durant nos discussions. Nous l'avons donc approfondi et avons abouti aux problèmes des polluants de l'eau : microplastiques, métaux lourds, phosphates et nitrates, déchets,... 

Dans l'idée de respecter la thème de Denis, nous nous sommes penchés sur les microplastiques.  

### 1.1 Que sont les microplastiques ? 

Les microplastiques sont des particules de plastique dont la taille est comprise entre 1 micromètre et 5 millimètres. Ils se présentent sous différentes formes de différentes tailles, filamenteux ou en granulés. Certains sont même tellement petits qu’ils sont invisibles à l’œil nu. Mais cela ne veut pas dire qu’ils ne font pas de dégâts. Effectivement, 90% de la pollution plastique des océans serait due aux microplastiques. Ces déchets peuvent également se retrouver dans les littoraux et les sédiments. Ils peuvent être ingérés (in)directement par des poissons, des mammifères marins mais aussi des oiseaux de mer et ainsi se retrouver dans des tissus vivants.  

Il existe deux catégories de microplastiques :  

- Les microplastiques primaires, qui sont des produits plastiques directement rejetés dans l’environnement sous forme de petites particules. Ils sont souvent utilisés dans les produits pharmaceutiques et cosmétiques mais également pour fabriquer des produits plastiques plus grands.  

- Les microplastiques secondaires, qui proviennent d’objets en plastique de plus grandes dimensions qui se fragmentent et se dégradent dans l’environnement. Ce sont les plus courants dans les environnements marins.   

Sur une échelle de taille, les microplastiques se situent entre les nanoplastiques mesurant de 1 à 999 nanomètres et les macroplastiques, supérieurs à 5 millimètres.  

### 1.2 La composition des microplastiques 

La composition des microplastiques peut aussi varier considérablement, car ils sont faits de nombreux types de plastiques différents et peuvent contenir divers additifs chimiques, comme des colorants ou des charges.  

Le plastique est composé de polymères organiques de synthèse obtenus par la polymérisation de monomères extraits du pétrole ou du gaz.  

Il existe énormément de polymères contenus dans ces microplastiques, en voici trois bien connus :  

|Types de polymère|Utilisations| 
|:-----------------:|:----:| 
|    Polyéthylène (PE) |**Basse densité** : bouteilles, jouets, sacs plastiques, sacs poubelle, revêtements, emballages, tubes pour le transport du gaz ou de l'eau. **Haute densité** : jouets, articles de ménage et de cuisine, isolants électriques, sacs plastiques, emballages alimentaires| 
|    Polypropylène (PP) |Récipients alimentaires type Tupperware, industrie automobile | 
|    Polystyrène (PS) |Emballages alimentaires, emballages de repas à emporter, gobelets de distributeurs automatiques, couverts en plastique, boites de CD | 

Malheureusement, ces propriétés ne permettent pas le bon recyclage de ce matériau, devenant ainsi une forme de pollution.  Ces minuscules morceaux de plastiques peuvent mettre des décennies, voire plus, à se dégrader complètement.  

### 1.3 Impact des microplastiques 

#### 1.3.1 Impact sur l'environnement

Maintenant que nous savons exactement de quoi nous parlons, nous pouvons nous poser davantage de questions.   

Nous sommes, en effet, tous conscient que les microplastiques sont nocifs pour notre santé, mais à quel point ? Y-a-t-il des études qui le prouvent ? Qu’en est-il des dégâts qu'ils pourraient causer sur l’environnement qui nous entoure ?    

Pour comprendre le réel problème et impact des microplastiques, nous nous sommes beaucoup documentés sur base d’articles scientifiques. Nous avons également trouvé des réponses à quelques-unes de nos questions auprès de Marine, notre mentor (dont nous parlerons par la suite).  

A l’heure actuelle, il est encore difficile pour les scientifiques d’évaluer avec précision la dangerosité de ceux-ci mais il est certain qu’ils ne sont pas à prendre à la légère.   

Les microplastiques sont présents partout. Ils peuvent être absorbés dans le corps humain par inhalation, en mangeant des aliments/animaux, eux-mêmes contaminés, mais également directement en buvant de l’eau potable.  

Selon des études sur les microplastiques dans l'air, l'eau, le sel et les fruits de mer, les enfants et les adultes pourraient ingérer entre des dizaines et plus de 100 000 particules de microplastiques chaque jour.  Dans le pire des cas, les gens pourraient ingérer chaque année l’équivalent d’une masse de microplastique équivalente à celle d’une carte de crédit !  

La mojorité des travaux sur les risques liés aux microplastiques ont été effectués sur des organisms marins. Le zooplancton, par exemple, parmi les plus petits organismes marins, croît plus lentement et se reproduit moins bien en présence de microplastiques. Les œufs des animaux sont plus petits et moins susceptibles d'éclore. Ses expériences montrent que les problèmes de reproduction proviennent du fait que le zooplancton ne mange pas suffisamment de nourriture.  

Le zooplancton exposé à des fibres microplastiques produit la moitié du nombre habituel de larves et que les adultes qui en résultent sont plus petits. Les fibres n’ont pas été ingérées, mais les chercheurs ont constaté qu’elles perturbent la nage et ont identifié des déformations dans le corps des organismes. L’exposition à ces fibres peuvent également réduire l’espérance de vie des crabes-taupes du Pacifique .   

Les océans sont déjà confrontés à de nombreux facteurs de stress. Les scientifiques craignent donc que les microplastiques n’épuisent davantage les populations de zooplancton qu’ils ne se propagent dans la chaîne alimentaire pour atteindre les humains.   « Si nous éliminons quelque chose comme le zooplancton, la base de notre réseau trophique marin, nous serions davantage préoccupés par les impacts sur les stocks de poissons et sur la capacité de nourrir la population mondiale. » 

#### 1.3.2 Impact sur la santé humaine 

Pour ce qui est de l’impact des microplastiques sur la santé humaine, un parallèle avec une étude menée sur des souris (Oral exposure to polyethylene microplastics alters gut morphology, immune response, and microbiota composition in mice (2022)) a révélé que l’ingestion de microplastiques par les souris cause des perturbations significatives au niveau de la structure, de la fonction et de la flore intestinale. Les résultats suggèrent que si ces polluants induisent des effets similaires chez l'homme, ils pourraient être associés à l'apparition de maladies telles que le cancer et les troubles inflammatoires.  
  

Les microparticules de PE, présentes dans notre alimentation, ont été liés à des altérations dans la paroi intestinale des souris après une alimentation contaminée pendant six semaines. Ces altérations comprennent une prolifération excessive des cryptes intestinales, responsables du renouvellement cellulaire, une altération de la fonction barrière de l'épithélium au niveau du côlon, essentielle à l'immunité intestinale, ainsi qu'une inflammation chronique locale favorisée par la surexpression de cytokines. De plus, certaines espèces bactériennes, comme les lactobacilles, protecteurs de la fonction intestinale, semblent être moins présentes dans le microbiote des souris exposées aux PE.  

Ces résultats soulèvent des inquiétudes quant à la potentielle menace pour la santé humaine. La chercheuse responsable de l'étude, Mathilde Body-Malapel, envisage plusieurs pistes de recherche futures. La première consiste à étudier l'impact des polluants plastiques dans des conditions de vie réelles, en examinant différentes formes de particules, en particulier celles non sphériques, et en analysant des mélanges de plastiques avec des additifs couramment utilisés dans les emballages alimentaires. Elle souligne également l'importance de comprendre les effets synergiques de ces différentes substances, à la manière d'un "effet cocktail".  

En outre, l'équipe de recherche prévoit d'explorer l'impact de ces microparticules sur des souris atteintes de maladies inflammatoires chroniques des intestins (MICI) et d'évaluer leur influence sur des échantillons d'épithélium humain, qu'ils soient sains ou affectés par les MICI, cultivés in vitro. 

De futures enquêtes visent à éclairer davantage sur une éventuelle causalité entre l'exposition aux microplastiques et le développement de maladies intestinales. Nous pouvons dors et déjà suspecter que les résultats seront inquiétants.  

### 1.4 Tree Analysis Processus 

Pour mieux comprendre le problème auquel nous avons décidé de nous attaquer, nous avons dessiné des arbres à problèmes puis, des arbres à solutions. 

![Arbres à problèmes et solutions](./images/Arbres%20.jpg) 

Au centre de notre arbre à problème nous avons placé **les microplastiques dans l'eau**. Les racines de l'arbres en représentent les causes: les pluies contaminées, le rejet de déchets plastiques par les industries, le manque de conscientisation,... 

Les branches représentent l'impact dû aux microplastiques dans l'eau. Ces conséquences sont le réchauffement climatique, l'induction de maladies,... 

Après avoir cerné les problèmes, nous avons établi l'arbre à solution. 

Cette fois, **l'assainissement des eaux** est placé au centre de notre arbre. Pour y parvenir il faudrait empêcher la combustion des déchets plastiques, conscientiser la population, filtrer l'eau,... 

Les conséquences émanentes sont, la disponibilité de l'eau potable, qui permet donc de s'altérer sans risque de contamination, et la disponibilité d'eau claire, pour les tâches quotidiennes : se laver, faire la vaisselle,... 

Maintenant que les causes et conséquences ont été établies, nous aimerions nous pencher sur une méthode d'assainissement de l'eau. Nous réfléchissons désormais à une méthode qui pourrait filtrer des micropastiques.  

## 2. Mentor et aide extérieure 

Pour nous aider tout au long de notre parcours, nous sommes partis à la recherche d'un mentor. Marine Pyl, doctorante en **Biologie marine**, a répondu positivement à notre demande. Nous l'avons donc rencontrée plusieurs fois et avons abordés quelques sujets importants. Elle nous a éclairés sur les pratiques de filtrations utilisées en laboratoire. Ces méthodes reposent sur l'aspiration de l'air à travers un filtre, entrainant une succion. Nous aurions plutôt imaginé une méthode inverse, basée sur la compression du liquide à filtrer.  

Elle nous a également parlé de l'impact néfaste qu'ont les microplastiques sur la santé en confiramant nos recherches bibliographiques préalables. Ces particules peuvent s'accumuler dans les organes et, à long terme, provoquer des cancers par inflammation. Cette toxicité proviendrait de la composition chimique à savoir, les additifs : le bisphénol A (perturbateur endocrinien), les pesticides, les métaux lourds,... 

Néanmoins, les études à ce sujet restent encore incomplètes et trop peu nombreuses à l'heure actuelle. 

Toute aide extérieur pouvant être bénéfique, nous avons également pris contact avec Xavier, le parrain d’Eva. Ce dernier, travaillant chez Vivaqua, nous a montré et expliqué chaque étape du Complexe de Tailfer. Il s’agit de l’unique captage d’eau de surface de Vivaqua. L’installation de production de Tailfer transforme l’eau brute de la Meuse en eau potable et assure quelques 30% de la production totale de Vivaqua. Malheureusement, une visite n’a pas pu être organisée due à la crise des PFAS de ces derniers temps. Cependant, nous avons tout de même eu l’opportunité de feuilleter le site de Vivaqua ci-joint, qui nous a permis de visualiser toutes les étapes mentionnées par Xavier.  

 

## 3. Construction de prototypes 

La première idée qui nous est venue à l'esprit, et la plus évidente à nos yeux, est un **filtre à eau**. 

D'abord, nous avons pensé à la conception d'un filtre multicouches. Ces dernières se seraient composées de graviers, de sable, de charbon et de coton.
Denis est alors intervenu et nous a dirigé vers un filtre à eau basé sur l'utilisation de bois.
En effet, le bois, composé de vaisseaux, est capable de transporter de la sève, indispensable à son métabolisme. Nous avons donc émis l'hypothèse que le bois pourrait tout aussi bien transporter de l'eau. Nos recherches nous ont apprises que ces vaisseaux conducteurs ont un diamètre allant de quelques nanomètres à 500 nm selon l'espèce. Cela voudrait donc dire que les particules dont le diamètre est supérieur, incluant des microplastiques, des pathogènes seraient retenues.
En tenant compte de la toxicité de certaines espèces et de la taille précise de leurs vaisseaux, nous nous sommes posés la question du type de bois à utiliser.
Grâce aux différents protoypes, nous allons rapidement comprendre qu'il y a plusieurs facteurs à prendre en compte: la toxicité, la taille des vaisseaux, la pression nécessaire,... 


### 3.1 Premiers prototypes 

 Pour notre première tentative, nous avons récolté du bois de *hêtre* et de *cyprès*. Nous les avons chacun polis et insérés dans le goulot d'une bouteille remplie d'un mélange d'eau et d'encre. Afin que le tout soit étanche, nous avons rajouté un tube en PVC et un ruban isolant. Pour que la gravité fasse effet, nous avons retourné nos bouteilles et effectué des trous dans les parties supérieures. 

 ![](./images/Prototype%201.jpg) 


 Après plusieurs heures, jours d'attente, aucune goute n'a été filtrée par le hêtre et ce malgré l'ajout de pression par nos mains. Quant au cyprès, nous avons observé une dizaine de gouttes mais de couleur jaune...

Grâce à quelques recherches supplémentaires, nous en sommes arrivés à la conclusion que le bois de feuillus tel que l'hêtre (composé de vaisseaux), n'est pas adapté à la filtration d'eau.
Par contre les conifères sont efficaces pour la filtration. Ceux-ci sont composés de trachéides dont le diamètre est compris entre 20µm à 80µm et peut donc filtrer les particules (dont les microplastiques, les pigments, ...) plus larges.
De plus, le cyprès s'avère toxique, ce qui pourrait expliquer la couleur jaunâtre des résidus, cette espèce est donc à éviter.

Pour ces raisons, nous avons décidé d'opter, lors de nos prochains prototypages, pour un gymnosperme, le Nordmann 

### 3.2 Deuxième prototype 

Pour ce deuxième essai, nous avons testé notre nouvelle espèce et, sans le savoir, un autre facteur, le vieillissement du bois.

En effet, nous avons débuté ce prototypage en répétant les étapes de la fois précédente. Nous avons cependant remplacé le bois par de le Nordmann et nous avons  apporté quelques modifications supplémentaires. Premièrement, l'ajout d'une chambre à air sur le dessus de la bouteille. Celle-ci nous a permis d'augmenter la pression à l'aide d'une pompe. Deuxièmement, le système manquant d'étanchéité, nous avons resserré les extrémités à l'aide de colsons. Après y avoir ajouté la pression, la magie opère! Du moins, d'un côté,...

Effectivement, les deux branches utilisées ont été récoltées à quelques jours d'intervalle. Le bois ayant été récolté le jour même a fonctionné comme nous l'espérions. Par contre, la branche, vieille de 5 jours, n'a pas filtré une seule goutte. 

![](./images/Premiere_goutte.jpg) 

![](./images/Filtre_oui_mais_non_2.jpg) 

Cet essai nous a permis de mettre le doigt sur plusieurs éléments importants.  

1. L'*epicéa* est efficace pour la filtration. 

2. Le bois doit être fraichement coupé pour faire effet. 

Cependant, le débit reste assez faible. 

### 3.3 Troisième prototype 

Pour optimiser notre prototype nous avons, cette fois-ci, tenté de comprendre le facteur de la pression. D’après nous l’ajout de pression amènerait à une augmentation du débit d’eau. Pour y arriver nous avons donc ajouter de la pression à l’aide d’une pompe. 

Nous avons voulu tester les limites de ce paramètre en y appliquant un maximum de pression. Cependant, cela à mené à l’explosion de notre prototype. En effet, un trop grand apport de pression n’est pas supporté par le système. 

![Alt text](<images/failcomplet (2).gif>)

Nous avons tenté de mesurer cette pression maximale mais, après trois tentatives, nous en avons conclu que notre pompe à vélo ne permet pas de mesurer avec suffisamment de précision. Il nous faudrait du matériel plus performant pour mesurer cette pression avec précision. 

La pression exacte à ajouter demeure donc un objectif à atteindre dans le futur.


### 3.4 Utilisation d'un outil du Fablab 

Ce qu'il nous manque désormais, c'est un support. Les outils du Fablab étant mis à notre disposition, nous avons décidé d'utiliser la découpeuse laser, la *Lasersaur*, appropriée à nos besoins. Pour cela, nous avons d'abord schématisé le prototype sur Inkscape pour ensuite le découper sur une planche en bois.  

![](./images/Support.jpg) 

## 4. Conclusion intermédiaire

Ces expériences ont prouvé plusieurs points. Premièrement, nous avons mis en évidence que tous les types de bois ne sont pas propices à la filtration. Les gymnospermes s’avèrent être plus adaptés, encore faut il tenir compte de leur éventuelle toxicité. 

Deuxièmement, le bois doit être fraîchement coupé pour être fonctionnel. Un bois sec, coupé il y a plusieurs jours, ne sera pas efficace.

Enfin, la pression est un facteur intéressant qui permet d’optimiser le débit. Même s’il n’a pas pu être mesuré avec précision, il est tout de même important à prendre ne compte si l’on veut un débit supérieur à quelques gouttes par heure.


Paramètre testé|Résultat| 
|:-----------------:|:----:| 
|    Pression   |   Débit de filtration plus élevé avec ajout de pression plutôt que pression atmosphérique seule  | 
|    Etanchéité des connexions|  Connexions plus étanches avec bandes de PTFE ou pièces du bon diamètre (morceaux de branche d'épicéa ou goulot de bouteille en plastique)  |  
|Particules à filtrer|**Avec de l'encre** : filtrat de couleur jaunâtre. **Avec du charbon actif** : léger dépôt noir au fond du récipient de collecte, sinon transparent| 


## 5. Feedback pré jury 

Lors du pré jury, nous avons présenté l’aboutissement de  notre projet, ou du moins, son avancement. La remarque prépondérante ressortant de notre présentation était le manque de précision du projet. En effet, contrairement à d’autres groupes, nous ne présentons pas un objet à des fins potentiellement commercialisables mais plutôt, un compte-rendu d’une compréhension approfondie du filtre en bois. Il nous a donc fallu restructurer nos idées en revenant quelques peu sur nos pas et en définissant de manière plus scientifiques les procédures mises en place pour chaque expérience, leurs objectifs, le matériel nécessaire,…  

Comme Denis nous l'a suffisamment répété, *Il faut tomber pour mieux rebondir*. Alors, même si la chute fut légèrement douloureuse, nous avons tenté d'améliorer tout ça en améliorant et en restructurant suivant une méthode plus scientifiques. 

Par la suite, nous allons donc synthétiser les connaissances sur la filtration par le bois en y intégrant à la fois nos recherches littéraires ainsi que la conclusion d'expériences personnelles supplémentaires. 

## 6. Recherches bibliographiques  


La pollution de l’eau en général est une problématique largement étudiée depuis de nombreuses années, ce qui nous à amené à parcourir de nombreux articles scientifiques et revues dans le but de nous approprier cette problématique. Dans notre recherche, nous nous sommes vite rendu compte que la pollution de l’eau au travers les microplastiques était un domaine encore mal compris et que des études plus approfondies vont encore devoir être menées afin de réellement comprendre la globalité des impact de se dérivé du pétrole.

Dans notre recherche documentaire, un article en particulier nous à particulièrement intéressé. Ce dernier intitulé « Water filtration using plant xylem » et publié en 2014, explique le potentiel que le xylème des plantes a pour répondre au besoin d'eau potable exempte de pathogènes dans les pays en développement et dans les environnements à ressources limitées. Bien que cette article cible un assainissement de l’eau à travers l’élimination de microorganismes, il nous à paru raisonnable de nous baser dessus au vue des nombreux avantages du bois, notamment sa disponibilité et sa facilité d’utilisation. Dans cette partie nous allons donc reprendre certaines informations issues de cet articles qui nous paraissaient pertinentes pour mener à bien notre projet.   

Avant de comprendre comment le xylème peut potentiellement filtrer, il est intéressant de comprendre la manière dont le xylème conduit la sève. La circulation de la sève dans les plantes est principalement due à la transpiration des feuilles vers l'atmosphère, ce qui crée une pression négative dans le xylème. L’absorption d’ions au niveau des racines crée également un gradient de pression permettant à la sève de remonter dans l’arbre. La structure du xylème comprend de nombreux petits conduits qui fonctionnent en parallèle et de manière à résister à la cavitation. Ce phénomène apparaît particulièrement lorsque la plante subit un stress hydrique modéré à élevé, amenant l'eau des conduits du xylème à se trouver sous très forte tension. La circulation ascendante de la sève est alors réduite, ce qui entraîne un déficit de croissance, voire la mort de la plante. Les conduits du xylème des gymnospermes (conifères) sont formés de cellules mortes simples et sont appelés trachéides, les plus grandes trachéides atteignant un diamètre de 80 mm et une longueur de 10 mm. Les angiospermes (plantes à fleurs) ont des conduits de xylème appelés vaisseaux qui sont dérivés de plusieurs cellules disposées en une seule file, ayant des diamètres allant jusqu'à 0,5 mm et des longueurs allant de quelques millimètres à plusieurs mètres. Ces dimensions peuvent varier considérablement entre les différentes espèces de plantes et même au sein d'une même espèce. Les dimensions spécifiques du xylème peuvent également être influencées par des facteurs environnementaux tels que la disponibilité de l'eau, la nutrition et d'autres conditions de croissance.  

Ces conduits parallèles ont des extrémités fermées et sont reliés aux conduits adjacents par des minuscules canaux. Ces pores nanométriques remplissent la fonction critique d'empêcher des bulles d’air de passer d'un conduit à l'autre car cela nuirait à la bonne circulation de la sève dans ces cavités. La porosité des membranes des fosses varie de quelques nanomètres à quelques centaines de nanomètres, la taille des pores étant généralement plus petite chez les angiospermes que chez les gymnospermes. 

Comme les angiospermes (plantes à fleurs, y compris les arbres feuillus) ont des vaisseaux de xylème plus grands qui sont plus efficaces pour conduire la sève, le tissu de xylème constitue une fraction plus petite de la surface de section transversale de leurs troncs ou branches, ce qui n'est pas idéal dans le contexte de la filtration. En revanche, les gymnospermes (conifères) ont des trachéides courtes qui forceraient l'eau à s'écouler à travers les pores des membranes, même pour de faibles épaisseurs de tissu xylémique. 

![Alt text](images/schematracheids.png)

Des expériences ont été menée pour caractérisé la filtration du bois à travers une formule pouvant déterminer le débit d’eau en fonction des paramètres du filtres. Il est en sorti la formule suivante :  

Q=KAdP/l 

Où Q est le débit volumétrique (en m³ s-1) sous la différence de pression dP à travers le filtre, tandis que l et A sont respectivement l'épaisseur et la surface de la section transversale du filtre. Le débit étant proportionnel à la différence de pression, il y été possible de trouver la conductivité hydrodynamique K, qui varie entre 5 et 6.10-10 m² Pa-1 s-1, où de manière équivalente 0,5 et 0,6  kg s-1 m-1 MPa-1 si on parle de débit massique. 

Dans la suite de notre projet, nous nous sommes penchés sur la caractérisation du bois utilisé pour mener à bien notre expérience afin mieux comprendre la manière dont il va interagir avec l’eau polluée qu’il va filtrer. Cette caractérisation s’est faite à travers de 3 expériences expliquées ci-dessous. 

## 7. Nos expériences scientifiques 


### 7.1 Première expérience 

- **But** : 

Le but de cette manipulation est de déterminer le lien, s’il existe, entre le débit d’eau en fonction de l’épaisseur du morceau de bois. 

D’après notre intuition nous aurions tendance à croire que le débit diminue selon l’épaisseur du bois. 

- **Matériel :**  

Branche de Nordmann , bouteille d’eau , ruban isolant , charbon en poudre, tube en PVC , pompe à vélo , chambre à air , fiole jaugée

![Alt text](images/7.1materiel.jpg)

- **Mode opératoire :** 

Découpez trois morceaux de bois à partir de la branche de Nordmann respectivement de 2cm, 3 cm, 4 cm. Placez chaque morceau dans le goulot du tube en pvc et rendez le tout hermétique à l’aide du ruban isolant. Placez la chambre à air dans le fond de la bouteille. Diluez le charbon dans de l’eau et incorporez le mélange à la bouteille. Maintenant, assemblez le tube en PVC et la bouteille et isolez le tout avec le ruban. Placez le tout au dessus d’un verre. Ajoutez de la pression dans la bouteille jusqu’à ce que vous sentiez une résistance maximale. Laissez le dispositif durant 1 heure puis mesurez le volume d’eau récolté. 

- **Observations :** 

Lors des trois essais, nous remarquons que le volume d’eau filtré obtenu est inférieur lorsque l’épaisseur du morceau de bois est plus importante.  

- **Conclusion :** 

D’après cette expérience, nous avons vérifié notre hypothèse selon laquelle l’épaisseur du bois influence le volume d’eau filtré. En effet, la vitesse de filtration est plus élevée pour du bois moins épais. Les données obtenues nous ont permises de réaliser un graphique de volume d’eau filtré selon l’épaisseur (représenté dans le point "Conclusion générale"). Ce graphique est d’apparence linéaire.  La vitesse d’écoulement est donc proportionnelle à la taille du bois utilisé. 

![Alt text](images/grapheaufiltr%C3%A9e.png)

### 7.2 Deuxième expérience 

- **But :** 

L’objectif de cette seconde expérience est de déterminer la durée de vie du bois après la cueillette de ce dernier. Avons-nous systématiquement besoin de couper du bois frais le jour de l’utilisation de notre filtre ou pouvons réutiliser du bois âgé ?
Notre hypothèse était que les vaisseaux d’un bois plus vieux seraient moins fonctionnels que ceux d’un bois plus jeune et par conséquent donnerait un débit plus faible. Pour s’en assurer, nous avons tenu de réaliser l’expérience suivante. 


- **Matériel :**   
1. un morceau de bois frais/cueillis le jour même

2. un morceau de bois coupé la veille

3. un morceau de bois coupé 2 jours avant

4. prototype habituel

- **Mode opératoire :** 

Pour chacun de ces bois, préparez le prototype avec un morceau de bois frais placé dans une des deux sortie et un morceau de bois coupé la veille dans l’autre. Laissez couler pendant une heure et récoltez l’eau afin d’en calculez le volume. Cela donne alors une approximation du débit d’eau. Réitérez l’expérience avec un morceau de bois datant de 2 jours et un autre de 3 jours

- **Résultats / Conclusion**  

Grâce à cet expérience nous avons constaté que les morceaux de bois âgés de trois jours n’étaient pas opérationnels. Aucune goutte d’eau n’est passée au travers des vaisseaux de xylème ! Il était donc inutile de continuer l’expérience avec des morceaux de bois encore plus vieux.

Quant aux morceaux de bois datant de la veille, nous avons obtenu moins de gouttes et un débit beaucoup plus faible que celui du bois frais.

Nous pouvons désormais confirmer notre hypothèse de départ. Plus le bois coupé est vieux, plus il sèche et plus il aura du mal à assurer ses fonctions de transport.

![](images/tableau_exp2.png)


### 7.3 Troisième expérience 

**But :** 

Le but de cette expérience est de déterminer combien de temps un filtre à bois est réutilisable en étant imprégnier d’eau quotidiennement. Cette expérience à par extension de la première expérience également permis de déterminer si il y avait un lien entre la longueur du bois et sa capacité à ne pas diminuer son débit après plusieurs utilisation.

**Matériel :** 

 Prototype habituel et morceau de branche de Nordmann

**Mode opératoire:**

Par facilité les morceaux de bois de l’expérience 1 ont été réutilisé pour mener a bien cette expérience. Pour chaque taille de bois différente, préparez le filtre avec et ajoutez le maximum de pression possible dans la bouteille. Mesurer le volume d’eau écoulée après une heure. Répétez l’opération le jour suivant jusqu’à obtention d’un débit nul. Attention, il faut garder en tête qu’il faudra rajouter de la pression après 30 minutes pour garder une différence de pression assez élevée pour permettre le débit.

**Observations :** Il semblerait qu'il y ait une relation inversement proportionnelle entre le débit d'eau filtré et la durée du moment « découpe-utilisation » 

**Résultats / conclusion :** 

Cette expérience a démontré que plus les jours passent, plus le débit diminue. Après 4 jours l’expérience a été arrêtée celle-ci car le débit était très proche de 0. Ces résultats sont sûrement dû au fait qu’au fil du temps, les vaisseaux de xylème ou les pores qui les relient s’obstruent par les saletés que ceux-ci retiennent lors du filtrage. On pourrait aussi penser que lorsqu’ils ne sont pas traverser d’eau, les vaisseaux ou les pores des morceaux de bois s’obstruent comme dans le cas de l’expérience 2. 

## 8. Conclusion générale 

Pour conclure, la filtration par le bois s’avère être une processus complexe, dépendant de plusieurs facteurs. Nos recherches bibliographiques ainsi que nos expériences nous ont permis de tirer les conclusions suivantes :

L’espèce dont provient le bois aura une influence sur la capacité de filtration de l’eau. En effet, plus les vaisseaux conducteurs auront un diamètre réduit, plus la gamme de rétention des microplastiques en fonction de leur taille sera élargie mais le revers de la médaille représente un débit réduit.
Les nanoplastiques ayant une taille plus petite que les pores reliant les vaisseaux conducteurs du bois servant de membrane semi-perméable passeront à travers pour se retrouver dans le filtrat.
La filtration gravitationnelle prend un temps considérable pour de maigres résultats et l’ajout de pression permet à l’eau contaminée de passer plus vite à travers le bois : il y a une proportionnalité entre le débit et la différence de pression.
 
L’expérience 1, qui analyse le débit en fonction de la longueur du morceau de bois filtrant, nous montre qu’il y a une relation inversement proportionnelle entre le débit volumique de l’eau et la longueur du morceau de bois. La longueur minimum pouvant être utilisée était de 2cm (taille inférieure n'est pas assez solidement retenu par le dispositif) au vu de notre dispositif et semble donc être la taille la plus adéquate pour obtenir un débit maximum.
 
L’expérience 2 cherchait à déterminer si du bois qui n’était pas coupé le jour même pouvait tout de même être utilisé comme filtre. Nous avons donc analyser le débit de sortie en fonction du temps depuis la découpe. Les morceaux de bois coupés ont été laisser à l’air libre et à température ambiante. Nous pouvons conclure de cette expérience que l’efficacité de filtrage, vu ici comme le débit, diminue lorsque le temps depuis la découpe augmente. En conclusion pour maximiser le débit de filtration, le bois doit être le plus fraichement coupé.  
 
L’expérience 3, proche de l’expérience 2, a été réalisée pour démontrer si des morceaux de bois utilisés quotidiennement pour la filtration, et qui restaient donc humide, avaient une durée de vie plus longue que ceux qui étaient simplement laisser à l’air libre. On en a retiré comme conclusion que le bois gardé humide a une efficacité plus grande que qu’un bois coupé au même moment mais qui serait resté à l’air libre entre le moment de la découpe et de son utilisation. Il faut cependant noté que l’efficacité des bois restés humides diminue tout de même significativement après la découpe. De ces deux dernière expériences, on peut tirer la conclusion suivante : moins la durée entre la découpe et l’utilisation du bois comme filtre est grande, plus le morceau de bois à une bonne efficacité de filtrage.


![Alt text](images/graphnordmann.png)


![](./images/Graphique%201.jpg) 

## 9. Gestion de projet 

Gérer un projet n’est jamais évident. Néanmoins nous avons mis en place plusieurs outils dans le but de mener à bien ce projet. 

Premièrement, l’ampleur d’un tel projet peut être effrayant. Pour mieux l’aborder, nous avons commencé par une hiérarchisation des tâches. Effectivement visualiser l’importance des tâches permet d’avoir une vue d’ensemble à la fois sur le travail à fournir et sur le temps à y consacrer. 

Il nous a esnuite été conseillé de diviser ces objectifs en plusieurs sous-tâches. Se fixer de petits objectifs attaignables est toujours plus motivant que de voir le projet dans son entièreté comme une montagne insurmontable. Nous avons gardé ce point en tête mais, au fur et à mesure du temps, par l’accumulation de retard, ces petits objectifs nous semblaient de moins en moins pertinents. Ce qui nous mène à notre point suivant, la gestion du temps. 

L'outil suivant consiste donc à préalablement évaluer le temps nécessaire à la réalisation de chaque tâche dans le but de limiter les retards. Au début, nous avions bien respecté cela mais plus tard, nous avons été dépassés par la charge de travail. Les retards se sont donc accumulés. Point à améliorer donc ! 

Avec le recul, nous aurions du insisté sur la gestion du temps car,  comme nous l’avons constaté, être dans les temps c’est déjà être en retard. L’accumulation de petits retards par-ci par-là nous a finalement rajouté une bonne dose de travail et de stress et, avant un blocus, c’est certainement à éviter ! 

Nous retenons donc l’importance de planifier l’avancement du projet en intégrant dès le début, des moments pour rattraper ces éventuels retards.  

| Problème |  Solution apportée  | 
|:-----------------:|:--------:|  
| Pas de créneau pour se voir  |  Réarrangement de nos plannings et jours de réunions fixés à mercredi + jeudi   |
| Absences   | Toujours envoyé un message de rappel sur le groupe |
| Retard aux réunions | La réunion commence d'office après 15min d'attente |
| Personnes en retrait | Animateur dirige la réunion et implique tous les membres|
|  Tout le monde parle en même temps | Bâton de parole et respect de la personne qui parle|   
|  Problème de communication lié à la plateforme utilisée  |   Création d'un groupe sur Messenger       |

## 10. Dynamique de groupe et projet 

 
La réussite d’un projet de groupe est évidemment liée à la dynamique de groupe. En effet, pour que notre équipe soit performante il nous a fallu prendre en compte plusieurs facteurs et évaluer nos forces et nos faiblesses.

Commençons d'abord par aborder nos points forts.

|Points forts| Avantages | 
|:-----------------:|:-----------------:| 
|La personnalité différente de chacun| Visions différentes, plusieurs points de vues permettent d'avancer efficacement|
| Bonne entente générale  | Le groupe est motivé à l'idée de se retrouver pour les réunions |  
| Egale répartition du travail  |Pas de surcharge|
| Respect mutuel  |Tout le monde est à l'aise, ose donner son avis|

La personnalité de chaque membre du groupe est un point fort, du moins si elle est mise en avant de la bonne manière. Nous avons vite remarqué, au sein de notre groupe, les personnalités plus fortes et plus timides. Pour que tout le monde se sente à l’aise, nous avons, dès le début, déterminé le rôle de chacun. Les rôle que nous nous sommes attribués sont : 

-	Animateur : Son rôle est d’établir le plan d’action en début de réunion. Il facilite également la communication en s’assurant que tout le monde participe et que l’avis de chacun soit pris en considération. Il guide la réunion d’après les objectifs fixés.
-	Ambianceur : Son rôle est de s’assurer que tout le monde se sente à l’aise et d’apporter la bonne humeur dès les premiers instants de la réunion
-	Secrétaire : Son rôle est de prendre note de tout ce qui se dit et, en fin de réunion, de résumer les points principaux.
-	Maître du temps : Son rôle est de s’assurer que la réunion est fluide, que nous ne passions pas trop de temps sur des détails. Il surveille le temps préalablement imparti pour chaque tâche.

Nous avons chacun classé ces rôles par ordre de préférence et en sommes arrivés à la conclusion que Timo serait l’animateur, Eva et Juliette alternent entre secrétaire et ambianceur et Armand gardera un œil sur le temps. De cette manière, tout le monde est à l’aise avec son rôle et la réunion se déroule de manière  optimale.

Lorsqu’on travaille en groupe, une bonne entente et le respect mutuel sont bien sûr deux choses importantes. Sur ce point nous avons de la chance car, dès le départ nous nous sommes bien entendus, avons rigolé ensemble et avons été ouvert aux idées de chacun. Cela nous a épargné de devoir travailler sur ce point. Sur le long terme cependant il y a  eu quelques frictions lorsque des avis divergeaient mais rien de grave, la bonne humeur a vite repris le dessus à chaque fois.

Un autre point fort de notre équipe est l’ égale répartition du travail et le respect de celle-ci. A la fin de chaque réunion, nous attribuions des tâches pour chaque membre. Ces missions individuelles ont toujours été réalisées dans les temps ou redistribuées si elles s’avéraient trop conséquentes.

Comme pour tout projet de groupe, nous avons rencontrés plusieurs difficultés et avons tenté d'y remédier

Les problèmes que nous avons rencontrés sont d’abord les absences. En effet, plusieurs fois il est arrivé que l’un ou l’autre membre manque à l’appel. Cela entraîne évidemment des complications et une prise de retard dû à un manque d’efficacité. Nous avons alors mis le doigt sur les causes principales de ces absences : l’oubli et disponibilités de chacun n'étaient pas assez prises en compte. Le groupe étant constitué de Bioingénieurs et d'un Biologiste, il a fallu prendre en compte les horaires de chacun et trouver des moment idéaux pour tous. Cela a représenté un challenge important car il n'a pas été évident de trouver des crénaux communs. Finalement nous avons tous tenté de déplacer d'autres évènement et nous sommes arivés à l'accord de se voir les mercredis matins et jeudis après-midis. Pour éviter les oublis de réunion, nous avons pris l'habitude de se rappeler la veille de chaque réunion l’heure et le lieu de rendez-vous. En cas de retard de plus de 15 minutes, ce qui est arrivé deux fois, la réunion commence sans perdre davantage de temps à attendre.

|Problèmes|Solutions|
|:-:|:-:|
|Mauvaise gestion du timing|Répartition du travail + Fixation d’objectifs à chaque réunion|
|Aucun budget|Cotisation pour le matériel nécessaire|
|Prise de retard sur la finalisation de notre projet|Coup de pouce durant la semaine qui a suivi le préjury|

La communication lors des réunion n’a pas toujours été évidente non plus. Certaines personnes avaient tendance à se taire plutôt qu’à exprimer leur ressenti. C’est dans ces moment que sont intervenus nos ambianceurs qui ont pris leur rôle bien à cœur. Parfois cela menait même à trop de discussions éparpillés et simultanées, dans ces moments-là, notre super animateur faisait intervention et notre bâton de parole prenait tout son sens. 

La communication post-réunion a aussi été compliqué au début. Nous avons testé plusieurs plateformes de communication (Discord, WhatsApp,…) pour aboutir à notre préférée : Messenger. L’information semble mieux passer sur cette application 😉

Globalement la dynamique de groupe a été efficace. Notre équipe est solide et a su se relever dans les moments difficiles. Nous avons pu remédier à chaque problème en y trouvant une solution adéquate. A la fin de ce projet, tout le monde a participé et est fier du travail accompli. Les difficultés rencontrées sont des leçons qui nous permettront d’être encore plus efficace lors de nos projets futurs. C’est donc ici que s’achève notre aventure ensemble ! 

A bientôt,

L’équipe Filtrao 

## Sources 

Water Filtration Using Plant Xylem (2014) : https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0089934

Oral exposure to polyethylene microplastics alters gut morphology, immune response, and microbiota composition in mice (2022) : https://www.sciencedirect.com/science/article/pii/S0013935122005576

Un jeune irlandais invente un aimant à microplastique pour nettoyer les océans (2019) : https://www.europeanscientist.com/fr/environnement/un-jeune-irlandais-invente-un-aimant-a-microplastiques-pour-nettoyer-les-oceans/

 Microplastics are everywhere – but are they harmful ? (2021) : https://www.nature.com/articles/d41586-021-01143-3

Objectifs de développement durable : https://www.un.org/sustainabledevelopment/fr/water-and-sanitation/

Bactéries mangeuses de plastiques, une bonne nouvelle pour la planète ? (2022)  : https://www.nationalgeographic.fr/environnement/bacteries-mangeuses-de-plastique-une-bonne-nouvelle-pour-la-planete

